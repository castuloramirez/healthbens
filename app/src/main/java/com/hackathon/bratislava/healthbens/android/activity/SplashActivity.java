package com.hackathon.bratislava.healthbens.android.activity;

import android.content.Intent;
import android.os.Bundle;
import com.hackathon.bratislava.healthbens.R;

public class SplashActivity extends android.app.Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread timer = new Thread(){
            public void run(){
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    changeActivity();
                }
            }
        };
        timer.start();

    }


    private void changeActivity(){
        Intent openStartingPoint = new Intent(this, LoginActivity.class);
        startActivity(openStartingPoint);
        SplashActivity.this.finish();
    }
}
