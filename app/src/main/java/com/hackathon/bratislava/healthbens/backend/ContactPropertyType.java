package com.hackathon.bratislava.healthbens.backend;

import java.io.Serializable;

/**
 * Created by mikedr on 08/04/2017.
 */
public class ContactPropertyType implements Serializable {
    private String name;
    private Boolean bonus;

    public ContactPropertyType() {

    }

    public ContactPropertyType(String name, Boolean bonus) {

        this.name = name;
        this.bonus = bonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactPropertyType)) return false;

        ContactPropertyType that = (ContactPropertyType) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return bonus != null ? bonus.equals(that.bonus) : that.bonus == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (bonus != null ? bonus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ContactPropertyType{" +
                "name='" + name + '\'' +
                ", bonus=" + bonus +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getBonus() {
        return bonus;
    }

    public void setBonus(Boolean bonus) {
        this.bonus = bonus;
    }
}
