package com.hackathon.bratislava.healthbens.android.fragments;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.hackathon.bratislava.healthbens.R;
import com.hackathon.bratislava.healthbens.backend.Contact;
import com.hackathon.bratislava.healthbens.backend.JSONUtils;
import com.hackathon.bratislava.healthbens.backend.Utils;

import java.util.ArrayList;


public class OneFragment extends Fragment {

    private View view;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one, container, false);

        Contact contact = JSONUtils.readContactFromAssets("contact.json", getActivity().getAssets());
        Float walkMonthReached = Utils.getGoalReachedWalkingMonth(contact);

        Integer walkMonthReachedInt = walkMonthReached.intValue();

        updateGraph(walkMonthReachedInt,100-walkMonthReachedInt);

        return view;
    }

    private void updateGraph(int finished, int toGo){
        PieChart walkingChart = (PieChart) view.findViewById(R.id.chart_walking);
        ArrayList<PieEntry> walkingEntries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        walkingEntries.add(new PieEntry(toGo));
        walkingEntries.add(new PieEntry(finished));

        PieDataSet walkingDataSet = new PieDataSet(walkingEntries, "");

        walkingDataSet.setDrawIcons(false);

        walkingDataSet.setSliceSpace(3f);
        walkingDataSet.setIconsOffset(new MPPointF(0, 40));
        walkingDataSet.setSelectionShift(5f);

        walkingDataSet.setColors(new int[]{getResources().getColor(R.color.windowBackground), getResources().getColor(R.color.colorPrimary)});
        //dataSet.setSelectionShift(0f);

        PieData walkingData = new PieData(walkingDataSet);
        walkingData.setValueFormatter(new PercentFormatter());
        walkingData.setValueTextSize(11f);
        walkingData.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(mTfLight);
        walkingChart.setData(walkingData);
        Description walkingDesc = new Description();
        walkingDesc.setText("");
        walkingChart.setDescription(walkingDesc);
        walkingChart.setDrawSliceText(false);
        // undo all highlights
        walkingChart.highlightValues(null);
        walkingChart.setHoleRadius(70f);

        walkingChart.invalidate();
        walkingChart.setTouchEnabled(false);
        walkingChart.animateY(1000);
        walkingChart.getLegend().setEnabled(false);
    }

}
