package com.hackathon.bratislava.healthbens.backend;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mikedr on 08/04/2017.
 */
public class Utils {
    public static List<Activity> getActivitiesByType(Contact contact, String activityName) {
        List<Activity> activityList = new LinkedList<>();

        if (contact == null || contact.getActivityList() == null)
            return activityList;

        for (Activity activity : contact.getActivityList()) {
            if (activity != null && activity.getActivityType() != null
                    && activity.getActivityType().getName() != null
                    && activity.getActivityType().getName().compareToIgnoreCase(activityName) == 0) {
                activityList.add(activity);
            }
        }
        return activityList;
    }

    public static List<Activity> getActivitiesByType(List<Activity> initialActivityList, String activityName) {
        List<Activity> activityList = new LinkedList<>();

        if (initialActivityList == null)
            return activityList;

        for (Activity activity : initialActivityList) {
            if (activity != null && activity.getActivityType() != null
                    && activity.getActivityType().getName() != null
                    && activity.getActivityType().getName().compareToIgnoreCase(activityName) == 0) {
                activityList.add(activity);
            }
        }
        return activityList;
    }

    public static List<Activity> getActivitiesSameDay(List<Activity> activityList) {
        List<Activity> filteredActivityList = new LinkedList<>();

        Date today = new Date();

        for (Activity activity : activityList) {
            if (DateUtils.isSameDay(today, activity.getDay())) {
                filteredActivityList.add(activity);
            }
        }

        return filteredActivityList;
    }

    public static List<Activity> getActivitiesSameWeek(List<Activity> activityList) {
        List<Activity> filteredActivityList = new LinkedList<>();

        Date today = new Date();

        for (Activity activity : activityList) {
            if (DateUtils.inSameWeek(today, activity.getDay())) {
                filteredActivityList.add(activity);
            }
        }

        return filteredActivityList;
    }

    public static List<Activity> getActivitiesSameMonth(List<Activity> activityList) {
        List<Activity> filteredActivityList = new LinkedList<>();

        Date today = new Date();

        for (Activity activity : activityList) {
            if (DateUtils.isSameMonth(today, activity.getDay())) {
                filteredActivityList.add(activity);
            }
        }

        return filteredActivityList;
    }

    public static List<Activity> getActivitiesSameYear(List<Activity> activityList) {
        List<Activity> filteredActivityList = new LinkedList<>();

        Date today = new Date();

        for (Activity activity : activityList) {
            if (DateUtils.inSameYear(today, activity.getDay())) {
                filteredActivityList.add(activity);
            }
        }

        return filteredActivityList;
    }

    public static List<Activity> getAggregatedActivities(List<Activity> activityList) {
        List<Activity> aggregatedActivityList = new LinkedList<>();

        for (String activityName : Constants.ACTIVITY_NAMES) {
            List<Activity> activityListByType = getActivitiesByType(activityList, activityName);
            if (activityListByType != null && !activityListByType.isEmpty()) {
                Activity currentActivity = new Activity(activityListByType.get(0).getActivityType(), new Date(), new Float(0));
                for (Activity activity : activityListByType) {
                    currentActivity.setDistance(currentActivity.getDistance() + activity.getDistance());
                }
                aggregatedActivityList.add(currentActivity);
            }
        }

        return aggregatedActivityList;
    }

    public static List<ContactPropertyType> getContactPropertyTypes(boolean bonus) {
        List<ContactPropertyType> contactPropertyTypeList = new LinkedList<>();
        for (ContactPropertyType type : Constants.CONTACT_PROPERTY_TYPES) {
            if (type.getBonus().equals(bonus)) {
                contactPropertyTypeList.add(type);
            }
        }
        return contactPropertyTypeList;
    }

    public static List<Float> getSavingPercantages(Contact contact) {
        List<Float> percentages = new LinkedList<>();

        for (int i = 0; i < 3; i++) {
            Float res = getPerc(contact, i);

            percentages.add(res);
        }

        return percentages;
    }

    private static Float getPerc(Contact contact, Integer periodType) {
        Float perc = new Float(0);
        Float distance = new Float(0);
        Float percGoal = new Float(0);
        List<Activity> activityList = contact.getActivityList();

        List<Activity> activityWalkList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_WALKING);
        List<Activity> activityRunList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_RUNNING);
        List<Activity> activityCycleList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_CYCLING);

        if (Constants.SAVING_DAY.equals(periodType)) {
            List<Activity> activityWalkDayList = Utils.getActivitiesSameDay(activityWalkList);
            List<Activity> aggWalkDayList = Utils.getAggregatedActivities(activityWalkDayList);
            Float distanceWalkDay = !aggWalkDayList.isEmpty() ? aggWalkDayList.get(0).getDistance() : new Float(0);
            List<Activity> activityRunDayList = Utils.getActivitiesSameDay(activityRunList);
            List<Activity> aggRunDayList = Utils.getAggregatedActivities(activityRunDayList);
            Float distanceRunDay = !aggRunDayList.isEmpty() ? aggRunDayList.get(0).getDistance() : new Float(0);
            List<Activity> activityCycleDayList = Utils.getActivitiesSameDay(activityCycleList);
            List<Activity> aggCycleDayList = Utils.getAggregatedActivities(activityCycleDayList);
            Float distanceCycleDay = !aggCycleDayList.isEmpty() ? aggCycleDayList.get(0).getDistance() : new Float(0);

            percGoal = getPercGoalReached(distanceWalkDay, distanceRunDay, distanceCycleDay, periodType);
        } else if (Constants.SAVING_MONTH.equals(periodType)) {
            List<Activity> activityWalkMonthList = Utils.getActivitiesSameMonth(activityWalkList);
            List<Activity> aggWalkMonthList = Utils.getAggregatedActivities(activityWalkMonthList);
            Float distanceWalkMonth = !aggWalkMonthList.isEmpty() ? aggWalkMonthList.get(0).getDistance() : new Float(0);
            List<Activity> activityRunMonthList = Utils.getActivitiesSameMonth(activityRunList);
            List<Activity> aggRunMonthList = Utils.getAggregatedActivities(activityRunMonthList);
            Float distanceRunMonth = !aggRunMonthList.isEmpty() ? aggRunMonthList.get(0).getDistance() : new Float(0);
            List<Activity> activityCycleMonthList = Utils.getActivitiesSameMonth(activityCycleList);
            List<Activity> aggCycleMonthList = Utils.getAggregatedActivities(activityCycleMonthList);
            Float distanceCycleMonth = !aggCycleMonthList.isEmpty() ? aggCycleMonthList.get(0).getDistance() : new Float(0);

            percGoal = getPercGoalReached(distanceWalkMonth, distanceRunMonth, distanceCycleMonth, periodType);
            percGoal = percGoal / 30;
        } else if (Constants.SAVING_YEAR.equals(periodType)) {
            List<Activity> activityWalkYearList = Utils.getActivitiesSameYear(activityWalkList);
            List<Activity> aggWalkYearList = Utils.getAggregatedActivities(activityWalkYearList);
            Float distanceWalkYear = !aggWalkYearList.isEmpty() ? aggWalkYearList.get(0).getDistance() : new Float(0);
            List<Activity> activityRunYearList = Utils.getActivitiesSameYear(activityRunList);
            List<Activity> aggRunYearList = Utils.getAggregatedActivities(activityRunYearList);
            Float distanceRunYear = !aggRunYearList.isEmpty() ? aggRunYearList.get(0).getDistance() : new Float(0);
            List<Activity> activityCycleYearList = Utils.getActivitiesSameYear(activityCycleList);
            List<Activity> aggCycleYearList = Utils.getAggregatedActivities(activityCycleYearList);
            Float distanceCycleYear = !aggCycleYearList.isEmpty() ? aggCycleYearList.get(0).getDistance() : new Float(0);

            percGoal = getPercGoalReached(distanceWalkYear, distanceRunYear, distanceCycleYear, periodType);
            percGoal = percGoal / 365;
        }

        perc = percGoal * Constants.MAX_SAVING / 100;
        return (perc > Constants.MAX_SAVING) ? Constants.MAX_SAVING : perc;
    }

    private static Float getPercGoalReached(Float walkDist, Float runDist, Float cycleDist, Integer periodType) {
        Float totalGoal;

        totalGoal = walkDist + (runDist/Constants.PERC_RUN_MULT) + (cycleDist/Constants.PERC_CYCLE_MULT);
        totalGoal = totalGoal - Constants.PERC_WALK_MIN;
        totalGoal = (totalGoal < 0) ? 0 : totalGoal;
        totalGoal = totalGoal / Constants.PERC_WALK_RANGE;

        return totalGoal * 100;
    }

    public static Float getGoalReachedWalkingMonth(Contact contact) {
        List<Activity> activityList = contact.getActivityList();

        List<Activity> activityWalkList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_WALKING);

        List<Activity> activityWalkMonthList = Utils.getActivitiesSameMonth(activityWalkList);
        List<Activity> aggWalkMonthList = Utils.getAggregatedActivities(activityWalkMonthList);
        Float distanceWalkMonth = !aggWalkMonthList.isEmpty() ? aggWalkMonthList.get(0).getDistance() : new Float(0);

        Float percGoal  = getPercGoalReached(distanceWalkMonth, new Float(0), new Float(0), Constants.SAVING_MONTH);
        percGoal = percGoal / 30; // month

        return percGoal;
    }

}
