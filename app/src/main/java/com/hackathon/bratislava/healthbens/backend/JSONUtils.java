package com.hackathon.bratislava.healthbens.backend;

import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Scanner;

/**
 * Created by mikedr on 08/04/2017.
 */
public class JSONUtils {
    public static Contact readContactFromJSONFile(String filename) {
        String content = null;
        try {
            content = new Scanner(new File(filename)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Gson gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd").setPrettyPrinting().create();

        Contact contact = gsonBuilder.fromJson(content, Contact.class);

        return contact;
    }

    public static Contact readContactFromAssets(String filename, AssetManager assetManager) {
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(filename);
        }
        catch (IOException e){
            Log.e("message: ",e.getMessage());
        }

        Writer writer = new StringWriter();
        char[] buffer = new char[8192];

        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        int n;
        try {
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Gson gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd").setPrettyPrinting().create();

        Contact contact = gsonBuilder.fromJson(writer.toString(), Contact.class);

        return contact;
    }

    public static Contact contact = null;
}
