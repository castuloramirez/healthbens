package com.hackathon.bratislava.healthbens.android.fragments.mvc;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hackathon.bratislava.healthbens.R;

public class Covid19Fragment extends Fragment {

    private Covid19ViewModel mViewModel;

    public static Covid19Fragment newInstance() {
        return new Covid19Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.covid19_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(Covid19ViewModel.class);
        // TODO: Use the ViewModel
    }

}
