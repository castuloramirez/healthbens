package com.hackathon.bratislava.healthbens.android.fragments;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.hackathon.bratislava.healthbens.R;
import com.hackathon.bratislava.healthbens.backend.Activity;
import com.hackathon.bratislava.healthbens.backend.Constants;
import com.hackathon.bratislava.healthbens.backend.Contact;
import com.hackathon.bratislava.healthbens.backend.JSONUtils;
import com.hackathon.bratislava.healthbens.backend.Utils;

import java.util.ArrayList;
import java.util.List;


public class TwoFragment extends Fragment{

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        setupActivityLog();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_two, container, false);
        PieChart savingsChart = (PieChart) view.findViewById(R.id.chart_savings);
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        Contact contact = JSONUtils.readContactFromAssets("contact.json", getActivity().getAssets());

        // LAST BIT OF CODING FOR THE MOCKUP
        List<Float> savingPerc = Utils.getSavingPercantages(contact);
        Integer savMonth = new Float(savingPerc.get(Constants.SAVING_MONTH)*100/Constants.MAX_SAVING).intValue();

        entries.add(new PieEntry(savMonth, "Saved money"));
        entries.add(new PieEntry(100-savMonth, "Amount you're paying"));

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        dataSet.setColors(new int[]{getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorPrimary)});
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(mTfLight);
        savingsChart.setData(data);
        Description desc = new Description();
        desc.setText("");
        savingsChart.setDescription(desc);
        savingsChart.setDrawSliceText(false);
        // undo all highlights
        savingsChart.highlightValues(null);

        savingsChart.invalidate();
        savingsChart.setTouchEnabled(false);
        //savingsChart.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        savingsChart.animateY(1000);
        return view;
    }

    private void setupActivityLog() {
        Contact contact = JSONUtils.contact;

        TableLayout tableLayout = (TableLayout) getView().findViewById(R.id.activity_table);

        TableRow tableRow = null;

        List<Activity> activityList = contact.getActivityList();

        TextView tv = null;

        //Filling in cells
        ////
        tableRow = (TableRow) tableLayout.findViewById(R.id.walk_row);
        List<Activity> activityWalkList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_WALKING);

        List<Activity> activityWalkDayList = Utils.getActivitiesSameDay(activityWalkList);
        List<Activity> aggWalkDayList = Utils.getAggregatedActivities(activityWalkDayList);
        Float distanceWalkDay = !aggWalkDayList.isEmpty() ? aggWalkDayList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.walk_day_cell);
        tv.setText(showDistance(distanceWalkDay));

        List<Activity> activityWalkMonthList = Utils.getActivitiesSameMonth(activityWalkList);
        List<Activity> aggWalkMonthList = Utils.getAggregatedActivities(activityWalkMonthList);
        Float distanceWalkMonth = !aggWalkMonthList.isEmpty() ? aggWalkMonthList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.walk_month_cell);
        tv.setText(showDistance(distanceWalkMonth));

        List<Activity> activityWalkYearList = Utils.getActivitiesSameYear(activityWalkList);
        List<Activity> aggWalkYearList = Utils.getAggregatedActivities(activityWalkYearList);
        Float distanceWalkYear = !aggWalkYearList.isEmpty() ? aggWalkYearList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.walk_year_cell);
        tv.setText(showDistance(distanceWalkYear));
        ////
        tableRow = (TableRow) tableLayout.findViewById(R.id.run_row);
        List<Activity> activityRunList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_RUNNING);

        List<Activity> activityRunDayList = Utils.getActivitiesSameDay(activityRunList);
        List<Activity> aggRunDayList = Utils.getAggregatedActivities(activityRunDayList);
        Float distanceRunDay = !aggRunDayList.isEmpty() ? aggRunDayList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.run_day_cell);
        tv.setText(showDistance(distanceRunDay));

        List<Activity> activityRunMonthList = Utils.getActivitiesSameMonth(activityRunList);
        List<Activity> aggRunMonthList = Utils.getAggregatedActivities(activityRunMonthList);
        Float distanceRunMonth = !aggRunMonthList.isEmpty() ? aggRunMonthList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.run_month_cell);
        tv.setText(showDistance(distanceRunMonth));

        List<Activity> activityRunYearList = Utils.getActivitiesSameYear(activityRunList);
        List<Activity> aggRunYearList = Utils.getAggregatedActivities(activityRunYearList);
        Float distanceRunYear = !aggRunYearList.isEmpty() ? aggRunYearList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.run_year_cell);
        tv.setText(showDistance(distanceRunYear));

        ////
        tableRow = (TableRow) tableLayout.findViewById(R.id.cycle_row);
        List<Activity> activityCycleList = Utils.getActivitiesByType(activityList, Constants.ACTIVITY_TYPE_CYCLING);

        List<Activity> activityCycleDayList = Utils.getActivitiesSameDay(activityCycleList);
        List<Activity> aggCycleDayList = Utils.getAggregatedActivities(activityCycleDayList);
        Float distanceCycleDay = !aggCycleDayList.isEmpty() ? aggCycleDayList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.cycle_day_cell);
        tv.setText(showDistance(distanceCycleDay));

        List<Activity> activityCycleMonthList = Utils.getActivitiesSameMonth(activityCycleList);
        List<Activity> aggCycleMonthList = Utils.getAggregatedActivities(activityCycleMonthList);
        Float distanceCycleMonth = !aggCycleMonthList.isEmpty() ? aggCycleMonthList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.cycle_month_cell);
        tv.setText(showDistance(distanceCycleMonth));

        List<Activity> activityCycleYearList = Utils.getActivitiesSameYear(activityCycleList);
        List<Activity> aggCycleYearList = Utils.getAggregatedActivities(activityCycleYearList);
        Float distanceCycleYear = !aggCycleYearList.isEmpty() ? aggCycleYearList.get(0).getDistance() : new Float(0);

        tv = (TextView) tableRow.findViewById(R.id.cycle_year_cell);
        tv.setText(showDistance(distanceCycleYear));

        //// % Saving Calculation
        List<Float> savingPerc = Utils.getSavingPercantages(contact);

        ////
        tableRow = (TableRow) tableLayout.findViewById(R.id.perc_row);

        tv = (TextView) tableRow.findViewById(R.id.saving_day_cell);
        tv.setText(showSavPerc(savingPerc.get(0)));

        tv = (TextView) tableRow.findViewById(R.id.saving_month_cell);
        tv.setText(showSavPerc(savingPerc.get(1)));

        tv = (TextView) tableRow.findViewById(R.id.saving_year_cell);
        tv.setText(showSavPerc(savingPerc.get(2)));

    }

    private String showSavPerc(Float perc) {
        perc = perc * 100;
        String formattedString = String.format("%.02f", perc);
        return formattedString + new String(" %");
    }

    private String showDistance(Float distance) {
        String formattedString = String.format("%.02f", distance);
        return formattedString + new String(" km");
    }

}
