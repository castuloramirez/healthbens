package com.hackathon.bratislava.healthbens.android.activity;

import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.hackathon.bratislava.healthbens.android.fragments.OneFragment;
import com.hackathon.bratislava.healthbens.android.fragments.ThreeFragment;
import com.hackathon.bratislava.healthbens.android.fragments.TwoFragment;

import java.util.ArrayList;
import java.util.List;


import com.hackathon.bratislava.healthbens.R;
import com.hackathon.bratislava.healthbens.android.util.Constants;
import com.hackathon.bratislava.healthbens.backend.Contact;
import com.hackathon.bratislava.healthbens.backend.JSONUtils;
import com.hackathon.bratislava.healthbens.backend.Utils;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icon_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        JSONUtils.contact = JSONUtils.readContactFromAssets("contact.json", getAssets());
    }

    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_tab_favourite,
                R.drawable.ic_tab_stats,
                R.drawable.ic_tab_contacts
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OneFragment(), "ONE");
        adapter.addFrag(new TwoFragment(), "TWO");
        adapter.addFrag(new ThreeFragment(), "THREE");
        getSupportActionBar().setTitle(getString(R.string.title_activity_Dashboard));
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                //here in array pass position as an index value for get title and set that
                if(position == Constants.DASHBOARD) {
                    getSupportActionBar().setTitle(getString(R.string.title_activity_Dashboard));
                }else if(position == Constants.ACTIVITY_LOG) {
                    getSupportActionBar().setTitle(getString(R.string.title_activity_Activity));
                }else if(position == Constants.PROFILE) {
                    getSupportActionBar().setTitle(getString(R.string.title_activity_Profile));
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int pos) {
                // TODO Auto-generated method stub

            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }

    public void onClickBtn(View v)
    {
        ImageButton icon = (ImageButton) findViewById(R.id.icon_activity);
        switch (v.getId()) {
            case R.id.btn_walking:
                icon.setImageResource(R.drawable.ic_walking);
                Contact contact = JSONUtils.readContactFromAssets("contact.json", this.getAssets());
                Float walkMonthReached = Utils.getGoalReachedWalkingMonth(contact);
                Integer walkMonthReachedInt = walkMonthReached.intValue();
                updateGraph(walkMonthReachedInt,100-walkMonthReachedInt);
                break;
            case R.id.btn_cycling:
                icon.setImageResource(R.drawable.ic_cycling);
                updateGraph(28,72);
                break;
            case R.id.btn_running:
                icon.setImageResource(R.drawable.ic_running);
                updateGraph(15,85);
                break;
        }
    }

    private void updateGraph(int finished, int toGo){
        PieChart walkingChart = (PieChart) findViewById(R.id.chart_walking);
        ArrayList<PieEntry> walkingEntries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        walkingEntries.add(new PieEntry(toGo));
        walkingEntries.add(new PieEntry(finished));

        PieDataSet walkingDataSet = new PieDataSet(walkingEntries, "");

        walkingDataSet.setDrawIcons(false);

        walkingDataSet.setSliceSpace(3f);
        walkingDataSet.setIconsOffset(new MPPointF(0, 40));
        walkingDataSet.setSelectionShift(5f);

        walkingDataSet.setColors(new int[]{getResources().getColor(R.color.windowBackground), getResources().getColor(R.color.colorPrimary)});
        //dataSet.setSelectionShift(0f);

        PieData walkingData = new PieData(walkingDataSet);
        walkingData.setValueFormatter(new PercentFormatter());
        walkingData.setValueTextSize(11f);
        walkingData.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(mTfLight);
        walkingChart.setData(walkingData);
        Description walkingDesc = new Description();
        walkingDesc.setText("");
        walkingChart.setDescription(walkingDesc);
        walkingChart.setDrawSliceText(false);
        // undo all highlights
        walkingChart.highlightValues(null);
        walkingChart.setHoleRadius(70f);

        walkingChart.invalidate();
        walkingChart.setTouchEnabled(false);
        walkingChart.spin( 200,0,-360f, Easing.EasingOption.EaseInOutQuad);
        walkingChart.getLegend().setEnabled(false);
    }

}
