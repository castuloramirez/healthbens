package com.hackathon.bratislava.healthbens.backend;

import java.io.Serializable;

/**
 * Created by mikedr on 08/04/2017.
 */
public class ContactProperty implements Serializable {
    private ContactPropertyType contactPropertyType;
    private Integer value;

    public ContactProperty() {

    }

    public ContactProperty(ContactPropertyType contactPropertyType, Integer value) {

        this.contactPropertyType = contactPropertyType;
        this.value = value;
    }

    @Override
    public String toString() {
        return "ContactProperty{" +
                "contactPropertyType=" + contactPropertyType +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactProperty)) return false;

        ContactProperty that = (ContactProperty) o;

        if (contactPropertyType != null ? !contactPropertyType.equals(that.contactPropertyType) : that.contactPropertyType != null)
            return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = contactPropertyType != null ? contactPropertyType.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    public ContactPropertyType getContactPropertyType() {

        return contactPropertyType;
    }

    public void setContactPropertyType(ContactPropertyType contactPropertyType) {
        this.contactPropertyType = contactPropertyType;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
