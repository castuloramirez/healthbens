package com.hackathon.bratislava.healthbens.backend;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by mikedr on 08/04/2017.
 */
public class Activity  implements Serializable {
    private ActivityType activityType;
    private Date day;
    private Float distance;

    public Activity() {

    }

    public Activity(ActivityType activityType, Date day, Float distance) {

        this.activityType = activityType;
        this.day = day;
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activityType=" + activityType +
                ", day=" + day +
                ", distance=" + distance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Activity)) return false;

        Activity activity = (Activity) o;

        if (activityType != null ? !activityType.equals(activity.activityType) : activity.activityType != null)
            return false;
        if (day != null ? !day.equals(activity.day) : activity.day != null) return false;
        return distance != null ? distance.equals(activity.distance) : activity.distance == null;
    }

    @Override
    public int hashCode() {
        int result = activityType != null ? activityType.hashCode() : 0;
        result = 31 * result + (day != null ? day.hashCode() : 0);
        result = 31 * result + (distance != null ? distance.hashCode() : 0);
        return result;
    }

    public ActivityType getActivityType() {

        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }
}
