package com.hackathon.bratislava.healthbens.backend;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by mikedr on 08/04/2017.
 */
public class Contact implements Serializable {
    private Date createDate;
    private String name;
    private Integer age;
    private String gender;
    private String phone;
    private String email;
    private String address;
    private List<ContactProperty> contactPropertyList;
    private List<Activity> activityList;

    public Contact() {
    }

    public Contact(Date createDate, String name, Integer age, String gender, String phone, String email, String address, List<ContactProperty> contactPropertyList) {

        this.createDate = createDate;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.contactPropertyList = contactPropertyList;
    }

    public Contact(Date createDate, String name, Integer age, String gender, String phone, String email, String address, List<ContactProperty> contactPropertyList, List<Activity> activityList) {
        this.createDate = createDate;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.contactPropertyList = contactPropertyList;
        this.activityList = activityList;
    }

    public List<Activity> getActivityList() {
        return activityList;
    }

    public void setActivityList(List<Activity> activityList) {
        this.activityList = activityList;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "createDate=" + createDate +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", contactPropertyList=" + contactPropertyList +
                ", activityList=" + activityList +
                '}';
    }

    public List<ContactProperty> getContactPropertyList() {

        return contactPropertyList;
    }

    public void setContactPropertyList(List<ContactProperty> contactPropertyList) {
        this.contactPropertyList = contactPropertyList;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
