package com.hackathon.bratislava.healthbens.backend;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by mikedr on 08/04/2017.
 */
public class Constants {

    // Activity Types
    public static final String ACTIVITY_TYPE_RUNNING = new String("Running");
    public static final String ACTIVITY_TYPE_WALKING = new String("Walking");
    public static final String ACTIVITY_TYPE_CYCLING = new String("Cycling");

    public static final LinkedList<String> ACTIVITY_NAMES = new LinkedList<>(Arrays.asList(
       ACTIVITY_TYPE_RUNNING, ACTIVITY_TYPE_WALKING, ACTIVITY_TYPE_CYCLING
    ));

    public static final LinkedList<ActivityType> ACTIVITY_TYPES = new LinkedList<>(Arrays.asList(
            new ActivityType(ACTIVITY_TYPE_RUNNING, "running.png"),
            new ActivityType(ACTIVITY_TYPE_WALKING, "walking.png"),
            new ActivityType(ACTIVITY_TYPE_CYCLING, "cycling.png")
    ));

    // Contact Property Types
    public static final String CONTACT_PROPERTY_TYPE_NAME_SMOKER = new String("Smoker");
    public static final String CONTACT_PROPERTY_TYPE_NAME_DRINKER = new String("Drinker");
    public static final String CONTACT_PROPERTY_TYPE_NAME_AN_ACTIVE = new String("An Active");
    public static final String CONTACT_PROPERTY_TYPE_NAME_CHR_DISEASE = new String("Chronic Disease");
    public static final String CONTACT_PROPERTY_TYPE_NAME_GEN_DISEASE = new String("Genetic Disease");
    public static final String CONTACT_PROPERTY_TYPE_NAME_MON_DV = new String("Monthly Doctor Visit");
    public static final String CONTACT_PROPERTY_TYPE_NAME_MICRO_CHIP = new String("Micro Chip Implant");

    public static final LinkedList<String> CONTACT_PROP_TYPE_NAMES = new LinkedList<>(Arrays.asList(
            CONTACT_PROPERTY_TYPE_NAME_SMOKER, CONTACT_PROPERTY_TYPE_NAME_DRINKER, CONTACT_PROPERTY_TYPE_NAME_AN_ACTIVE,
            CONTACT_PROPERTY_TYPE_NAME_CHR_DISEASE, CONTACT_PROPERTY_TYPE_NAME_GEN_DISEASE, CONTACT_PROPERTY_TYPE_NAME_MON_DV,
            CONTACT_PROPERTY_TYPE_NAME_MICRO_CHIP
    ));

    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_SMOKER = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_SMOKER, false);
    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_DRINKER = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_DRINKER, false);
    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_AN_ACTIVE = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_AN_ACTIVE, false);
    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_CHR_DISEASE = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_CHR_DISEASE, false);
    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_GEN_DISEASE = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_GEN_DISEASE, false);
    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_MON_DV = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_MON_DV, true);
    public static final ContactPropertyType CONTACT_PROPERTY_TYPE_MICRO_CHIP = new ContactPropertyType(CONTACT_PROPERTY_TYPE_NAME_MICRO_CHIP, true);

    public static final LinkedList<ContactPropertyType> CONTACT_PROPERTY_TYPES = new LinkedList<>(Arrays.asList(
            CONTACT_PROPERTY_TYPE_SMOKER,
            CONTACT_PROPERTY_TYPE_DRINKER,
            CONTACT_PROPERTY_TYPE_AN_ACTIVE,
            CONTACT_PROPERTY_TYPE_CHR_DISEASE,
            CONTACT_PROPERTY_TYPE_GEN_DISEASE,
            CONTACT_PROPERTY_TYPE_MON_DV,
            CONTACT_PROPERTY_TYPE_MICRO_CHIP
    ));

    public static final Float MAX_SAVING = new Float(0.05);

    public static final Float PERC_WALK_MIN = new Float(1);
    public static final Float PERC_WALK_RANGE = new Float(6);
    public static final Float PERC_RUN_MULT = new Float(1.75);
    public static final Float PERC_CYCLE_MULT = new Float(2.8);

    public static final Integer SAVING_DAY = new Integer(0);
    public static final Integer SAVING_MONTH = new Integer(1);
    public static final Integer SAVING_YEAR = new Integer(2);

}
