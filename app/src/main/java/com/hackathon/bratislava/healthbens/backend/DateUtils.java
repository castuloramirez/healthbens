package com.hackathon.bratislava.healthbens.backend;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by mikedr on 08/04/2017.
 */
public class DateUtils {
    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(date1);
        cal2.setTime(date2);

        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

        return sameDay;
    }

    public static boolean inSameWeek(Date date1, Date date2) {
        if (null == date1 || null == date2) {
            return false;
        }

        Calendar earlier = Calendar.getInstance();
        Calendar later = Calendar.getInstance();

        if (date1.before(date2)) {
            earlier.setTime(date1);
            later.setTime(date2);
        } else {
            earlier.setTime(date2);
            later.setTime(date1);
        }
        if (inSameYear(date1, date2)) {
            int week1 = earlier.get(Calendar.WEEK_OF_YEAR);
            int week2 = later.get(Calendar.WEEK_OF_YEAR);
            if (week1 == week2) {
                return true;
            }
        } else {
            int dayOfWeek = earlier.get(Calendar.DAY_OF_WEEK);
            earlier.add(Calendar.DATE, 7 - dayOfWeek);
            if (inSameYear(earlier.getTime(), later.getTime())) {
                int week1 = earlier.get(Calendar.WEEK_OF_YEAR);
                int week2 = later.get(Calendar.WEEK_OF_YEAR);
                if (week1 == week2) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isSameMonth(Date date1, Date date2) {
        //Create 2 intances of calendar
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(date1);
        cal2.setTime(date2);

        if(cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) {
            if(cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) {
                return true;
            }
        }

        return false;
    }

    public static boolean inSameYear(Date date1, Date date2) {
        if (null == date1 || null == date2) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        int year1 = cal1.get(Calendar.YEAR);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 == year2)
            return true;

        return false;
    }
}
