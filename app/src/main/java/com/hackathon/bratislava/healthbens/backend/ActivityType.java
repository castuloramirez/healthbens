package com.hackathon.bratislava.healthbens.backend;

import java.io.Serializable;

/**
 * Created by mikedr on 08/04/2017.
 */
public class ActivityType implements Serializable {
    private String name;
    private String graphic;

    public ActivityType() {

    }

    public ActivityType(String name) {

        this.name = name;
    }

    public ActivityType(String name, String graphic) {

        this.name = name;
        this.graphic = graphic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActivityType)) return false;

        ActivityType that = (ActivityType) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return graphic != null ? graphic.equals(that.graphic) : that.graphic == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (graphic != null ? graphic.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ActivityType{" +
                "name='" + name + '\'' +
                ", graphic='" + graphic + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGraphic() {
        return graphic;
    }

    public void setGraphic(String graphic) {
        this.graphic = graphic;
    }
}
